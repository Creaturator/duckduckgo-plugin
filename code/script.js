 for (let resultElem of document.getElementsByClassName('result__body')) {
    const linkElem = resultElem.getElementsByClassName('result__url')[0];
    const link = "https://" + linkElem.innerText;
    linkElem.href = link;
    for(let aElem of resultElem.getElementsByTagName('a'))
        aElem.href = link;
}